import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { NavController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth'
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  user = {} as User;
  cpassword: string;
  constructor(public navCtrl: NavController, private afAuth: AngularFireAuth) { }
  back(){
    this.navCtrl.navigateForward('login')
  }
  async register(user: User){
    if(user.password === this.cpassword && user.email.length!==0 && user.password.length!==0 && this.cpassword.length!==0){
       try{
      const result = await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
      this.navCtrl.navigateForward('home');
        }catch (e){
      alert(e);
    }
    }
   
  }
  ngOnInit() {
  }

}
