import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { NavController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth'
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user = {} as User;
  
  constructor(public navCtrl: NavController, private afAuth: AngularFireAuth) { }

  async login(user: User){
   try {
    const result = this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)
    if(result){
    this.navCtrl.navigateForward('home')
    }
   } catch (e){
     alert(e)
   }
  }

  register(){
    this.navCtrl.navigateForward('register')
  }
  ngOnInit() {
  }

}
